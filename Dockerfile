FROM maven:onbuild
CMD ["java", "-jar", "/usr/src/app/target/demo-0.0.1-SNAPSHOT.jar", "--server.port=8081"]