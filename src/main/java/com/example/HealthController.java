package com.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.log4j.Logger;
import org.apache.logging.log4j.core.util.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Yuriy Aizenberg
 */
@Controller("/")
public class HealthController {

    private static final Logger LOGGER = Logger.getLogger(HealthController.class);


    @RequestMapping(value = "/health", method = RequestMethod.POST)
    public
    @ResponseBody
    RObject apply(@RequestBody String message) {
        LOGGER.info(message);
        return new RObject();
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public void get(HttpServletResponse response) {
        response.setContentType("text/html");
        try {
            File file = new File("./application.log");
            ServletOutputStream outputStream = response.getOutputStream();
            if (!file.exists()) {
                outputStream.write("FileNotFound".getBytes());
            } else {
                IOUtils.copy(new FileReader(file), new BufferedWriter(new OutputStreamWriter(outputStream)));
            }
            response.flushBuffer();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static class RObject {
        @JsonProperty
        int param = 0;
    }
}
